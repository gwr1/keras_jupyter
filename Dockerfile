FROM nvidia/cuda:8.0-cudnn6-devel-ubuntu16.04

RUN apt-get update && apt-get install -y --no-install-recommends \
        build-essential \
        curl \
        git \
        libcurl3-dev \
        libfreetype6-dev \
        libpng12-dev \
        libzmq3-dev \
        pkg-config \
        python-dev \
        rsync \
        software-properties-common \
        unzip \
        zip \
        zlib1g-dev \
        openjdk-8-jdk \
        openjdk-8-jre-headless \
        wget 

RUN curl -fSsL -O https://bootstrap.pypa.io/get-pip.py && \
    python get-pip.py && \
    rm get-pip.py
    
RUN pip install --upgrade pip

RUN pip --no-cache-dir install \
        ipykernel \
        jupyter \
        matplotlib \
        numpy \
        scipy \
        sklearn \
        pandas \
        && \
    python -m ipykernel.kernelspec

# Set up our notebook config.
RUN mkdir .jupyter
COPY jupyter_notebook_config.py /root/.jupyter/

# Jupyter has issues with being run directly:
#   https://github.com/ipython/ipython/issues/7062
# We just add a little wrapper script.
COPY run_jupyter.sh /

# add repos
RUN echo "deb [arch=amd64] http://storage.googleapis.com/bazel-apt stable jdk1.8" | tee /etc/apt/sources.list.d/bazel.list && \
    curl https://bazel.build/bazel-release.pub.gpg | apt-key add - && \
    apt-get update -qq && apt-get upgrade -y -qq && apt-get install --no-install-recommends -y \
    apt-transport-https \
    wget \
    git \
    vim \
    libcupti-dev \
    sshfs

# Anaconda3 install
ENV PATH /opt/conda/bin:$PATH
RUN echo 'export PATH=/opt/conda/bin:$PATH' >> /etc/profile.d/conda.sh
RUN wget --quiet https://repo.continuum.io/archive/Anaconda3-4.3.1-Linux-x86_64.sh -O ~/anaconda.sh
RUN /bin/bash ~/anaconda.sh -b -p /opt/conda && \
    rm ~/anaconda.sh

# ODBC install

RUN apt-get -y install freetds-dev freetds-bin unixodbc-dev tdsodbc

RUN echo '[FreeTDS]\n' >> /etc/odbcinst.ini
RUN echo 'Description=FreeTDS Driver\n' >> /etc/odbcinst.ini
RUN echo 'Driver=/usr/lib/x86_64-linux-gnu/odbc/libtdsodbc.so\n' >> /etc/odbcinst.ini
RUN echo 'Setup=/usr/lib/x86_64-linux-gnu/odbc/libtdsS.so\n' >> /etc/odbcinst.ini


# Set up Bazel.

# Running bazel inside a `docker build` command causes trouble, cf:
#   https://github.com/bazelbuild/bazel/issues/134
# The easiest solution is to set up a bazelrc file forcing --batch.
RUN echo "startup --batch" >>/etc/bazel.bazelrc
# Similarly, we need to workaround sandboxing issues:
#   https://github.com/bazelbuild/bazel/issues/418
RUN echo "build --spawn_strategy=standalone --genrule_strategy=standalone" \
    >>/etc/bazel.bazelrc
# Install the most recent bazel release.
ENV BAZEL_VERSION 0.5.0
WORKDIR /
RUN mkdir /bazel && \
    cd /bazel && \
    curl -H "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36" -fSsL -O https://github.com/bazelbuild/bazel/releases/download/$BAZEL_VERSION/bazel-$BAZEL_VERSION-installer-linux-x86_64.sh && \
    curl -H "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36" -fSsL -o /bazel/LICENSE.txt https://raw.githubusercontent.com/bazelbuild/bazel/master/LICENSE && \
    chmod +x bazel-*.sh && \
    ./bazel-$BAZEL_VERSION-installer-linux-x86_64.sh && \
    cd / && \
    rm -f /bazel/bazel-$BAZEL_VERSION-installer-linux-x86_64.sh

RUN pip install tensorflow-gpu

RUN wget https://bootstrap.pypa.io/ez_setup.py -O - | python
# RUN conda remove tensorflow
RUN conda install -y libgcc

WORKDIR /root

# TensorBoard
EXPOSE 6006
# IPython
#EXPOSE 8888

#RUN ["/bin/bash"]


# keras
# ARG KERAS_VERSION=2.0.5
ENV KERAS_BACKEND=tensorflow

RUN pip --no-cache-dir install --no-dependencies git+https://github.com/fchollet/keras.git


RUN conda install -y protobuf
# make jyputer hacks
RUN git clone https://github.com/PAIR-code/facets facets-dist && \
    jupyter nbextension install facets-dist/ && \
    rm -r facets-dist

RUN mkdir -p ~/.ipython/profile_default/
RUN touch ~/.ipython/profile_default/ipython_config.py
RUN echo 'c = get_config()\n' >> ~/.ipython/profile_default/ipython_config.py
# Run all nodes interactively
RUN echo 'c.InteractiveShell.ast_node_interactivity = "all"\n' >> ~/.ipython/profile_default/ipython_config.py
RUN echo 'c.InlineBackend.figure_format = "retina"\n' >> ~/.ipython/profile_default/ipython_config.py
RUN echo 'c.InteractiveShellApp.extensions = ["autoreload"]\n' >> ~/.ipython/profile_default/ipython_config.py
RUN echo 'c.InteractiveShellApp.exec_lines = ["%autoreload 2"]\n' >> ~/.ipython/profile_default/ipython_config.py
RUN echo 'c.NotebookApp.iopub_data_rate_limit=10000000000000\n' >> ~/.ipython/profile_default/ipython_config.py

# presentation from jupyter
RUN pip --no-cache-dir install RISE
RUN jupyter-nbextension install rise --py --sys-prefix
RUN jupyter-nbextension enable rise --py --sys-prefix

# sql magic for jupyter
RUN pip --no-cache-dir install ipython-sql

# # install ipyparallel
# RUN pip install ipyparallel
# RUN ipcluster nbextension enable

#install graphviz
RUN apt-get install -y graphviz && pip install graphviz

# installs
RUN pip install pydataset statsmodels filterpy pyodbc sqlalchemy tensor2tensor ml_metrics pydot-ng
# ml_metricks

# install gym
RUN apt-get install -y cmake
RUN pip install gym gym[atari] atari_py
#install opencv
# RUN apt-get install -y ffmpeg
# RUN conda install -y opencv
# RUN pip install opencv-contrib-python
# https://github.com/BVLC/caffe/wiki/OpenCV-3.2-Installation-Guide-on-Ubuntu-16.04
RUN apt-get update
RUN apt-get install --assume-yes build-essential cmake git
RUN apt-get install --assume-yes pkg-config unzip ffmpeg qtbase5-dev python-dev python3-dev python-numpy python3-numpy
RUN apt-get install --assume-yes libopencv-dev 
#RUN apt-get install --assume-yes libgtk-3-dev 
RUN apt-get install --assume-yes libdc1394-22 
RUN apt-get install --assume-yes libdc1394-22-dev 
RUN apt-get install --assume-yes libjpeg-dev 
RUN apt-get install --assume-yes libpng12-dev 
RUN apt-get install --assume-yes libtiff5-dev 
RUN apt-get install --assume-yes libjasper-dev
RUN apt-get install --assume-yes libavcodec-dev libavformat-dev libswscale-dev libxine2-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev
RUN apt-get install --assume-yes libv4l-dev libtbb-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev
RUN apt-get install --assume-yes libvorbis-dev libxvidcore-dev v4l-utils python-vtk
RUN apt-get install --assume-yes liblapacke-dev libopenblas-dev checkinstall
RUN apt-get install --assume-yes libgdal-dev

RUN echo 'c.InteractiveShellApp.matplotlib = "inline"\n' >> ~/.ipython/profile_default/ipython_config.py
# RUN wget https://github.com/opencv/opencv/archive/c.InteractiveShellApp.matplotlib = "inline"3.2.0.zip && unzip 3.2.0.zip && rm 3.2.0.zip
# RUN cd opencv-3.2.0 && \
#             mkdir build && \
#             cd build/ && \
#             cmake -DCMAKE_BUILD_TYPE=RELEASE -DCMAKE_INSTALL_PREFIX=/usr/local \
#                   -DCUDA_CUDA_LIBRARY=/usr/local/cuda/targets/x86_64-linux/lib/stubs/libcuda.so \
#                   -DFORCE_VTK=ON -DWITH_TBB=ON \
#                   -DWITH_V4L=ON -DWITH_QT=ON -DWITH_OPENGL=ON -DWITH_CUBLAS=ON -DCUDA_NVCC_FLAGS="-D_FORCE_INLINES" \
#                   -DWITH_GDAL=ON -DWITH_XINE=ON -DBUILD_EXAMPLES=ON \
#                   -DENABLE_PRECOMPILED_HEADERS=OFF ..
# RUN cd opencv-3.2.0/build && \
#             make -j $(($(nproc) + 1))
# RUN cd opencv-3.2.0/build && \
#             make install && \
#             /bin/bash -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf' && \
#             CUDA_CUDA_LIBRARY=/usr/local/cuda/targets/x86_64-linux/lib/stubs/libcuda.so ldconfig && \
#             apt-get update && \
#             cd ~/ && \
#             rm -r opencv-3.2.0
#RUN git clone https://go.googlesource.com/go && cd go && git checkout go1.8.3 && cd src && ./all.bash
RUN pip install virtualenv
RUN apt-get install -y vowpal-wabbit

RUN pip install git+https://gwr1@bitbucket.org/gwr1/mssqlconnect.git#egg=mssqlconnect
RUN pip install git+https://gwr1@bitbucket.org/gwr1/readsql.git#egg=readsql

#RUN pip install --upgrade pip
#RUN pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U
RUN conda update --all

RUN apt-get -y install enchant
RUN pip install pyenchant
RUN conda install -y -c conda-forge jupyterlab
RUN pip install jupyterlab
RUN jupyter serverextension enable --py jupyterlab --sys-prefix
RUN pip install asn1crypto appdirs autocorrect boto3 botocore cfscrape colour datasource entrypoints gplearn hmmlearn imageio jmespath joblib jupyterlab jupyterlab-launcher Mako mccabe mysql-connector olefile 
#RUN conda install -y opencv 
#RUN conda install -y Lasagne 
RUN pip install opencv-python pandocfilters parso pycodestyle pycuda PyExecJS pydotplus pyenchant PyOpenGL pytools rope ruamel.yaml s3transfer selenium simplejson sklearn-pandas sphinxcontrib-websupport t-sne-bhcuda tensorflow-gpu testpath Theano torch torchvision webencodings wiki-ru-wordnet xlwt yahoo-finance transliterate 
#RUN pip install spacy telnet

RUN pip install spacy
RUN python -m spacy download en
RUN python -m spacy download xx 
#RUN pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U

RUN git clone https://github.com/Yvictor/TradingGym.git && cd TradingGym && python setup.py install

#RUN apt-get install -y libboost-program-options-dev libboost-python-dev libtool m4 automake
#RUN git clone git://github.com/JohnLangford/vowpal_wabbit.git && cd vowpal_wabbit && ./autogen.sh && ./configure && make
RUN apt autoremove -y
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# IPython
EXPOSE 8888

# CMD ["ipcluster","start"]
# ADD ./start.sh /start.sh
# RUN chmod 755 /start.sh
# CMD ["/bin/bash", "/start.sh"]
RUN mkdir /notebooks /repos /logs
WORKDIR /notebooks

COPY start.sh /
COPY jupyter_notebook_config.py /
RUN chmod +x /run_jupyter.sh /start.sh
#--NotebookApp.iopub_data_rate_limit=10000000000
#CMD ["/start.sh"]
CMD ["/run_jupyter.sh","--NotebookApp.iopub_data_rate_limit=100000000000","--allow-root"]